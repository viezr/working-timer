'''
Utils module.
'''
import time
from datetime import date


def format_seconds(seconds: [int, float]) -> str:
    '''
    Return seconds as string in H:M:S format.
    '''
    if seconds <= 0:
        return "--:--:--"
    time_inst = time.gmtime(seconds)
    time_out = time.strftime("%H:%M:%S", time_inst)
    return time_out

def parse_time(time_hms: str) -> int:
    '''
    Parse time from user input.
    '''
    try:
        time_struct = time.strptime(time_hms, "%H:%M:%S")
        seconds = (time_struct.tm_hour * 3600 + time_struct.tm_min *
            60 + time_struct.tm_sec)
        return seconds
    except (ValueError, TypeError) as err:
        error = f"Wrong time format."
        raise ValueError(error)

def parse_import_file(data: str, prj_db_titles: list) -> [dict, str]:
    '''
    Parse import file and return dict of projects with times.
    '''
    projects_dict = {}
    error = ""
    error_head = "Import error. Line: "
    for idx, line in enumerate(data):
        line = line.strip()
        if not line:
            continue
        prj_title, prj_date, prj_sec = _parse_import_line(line, idx)
        if prj_title in prj_db_titles:
            error = ''.join([error_head, str(idx), ". Project exists."])
            raise ValueError(error)
        if not prj_title in projects_dict:
            projects_dict[prj_title] = {}
        if prj_date in projects_dict[prj_title]:
            error = ''.join([error_head, str(idx), ". Date dublication ",
                str(prj_date), ". For project '", prj_title, "'."])
            raise ValueError(error)
        projects_dict[prj_title][prj_date] = int(prj_sec)
    return projects_dict

def _parse_import_line(line: str, idx: int) -> tuple:
    '''
    Parse imported line and return tuple with project name, date and seconds.
    '''
    error_head = "Import error. Line: "
    try:
        prj_title, prj_date, prj_sec = line.split(",")
        prj_title = prj_title.strip()
    except IndexError as err:
        error = ''.join([error_head, str(idx),
            " expected 3 parts with comma separator."])
        raise IndexError(error)
    if len(prj_title) < 2:
        error = ''.join([error_head, str(idx), ". Minimum 2 chars in title."])
        raise ValueError(error)
    try:
        prj_date = date.fromisoformat(prj_date.strip())
    except (TypeError, ValueError) as err:
        error = ''.join([error_head, str(idx), ". Wrong date format."])
        raise ValueError(error)
    prj_sec = prj_sec.strip()
    try:
        prj_sec = int(prj_sec)
    except (TypeError, ValueError) as err:
        error = ''.join([error_head, str(idx), ". Wrong seconds format."])
        raise ValueError(error)
    return (prj_title, prj_date, prj_sec)
