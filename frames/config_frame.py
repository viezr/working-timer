'''
Config frame.
'''
import tkinter as tk
from tkinter import ttk
from tkinter import filedialog as fd

from .utils import format_seconds, parse_import_file


class ConfigFrame(tk.Frame):
    '''
    Config frame class.
    '''
    def __init__(self, root, container):
        self.root, self.container = root, container
        tk.Frame.__init__(self, self.container)
        self._colors = self.root.style.colors
        self.configure(bg=self._colors["bg"])
        self.active = False
        self._next_row = 0
        self._prev_prj_title = None
        self._prev_prj_set_btn = None
        self._prev_prj_del_btn = None

        self._fill()

    def _fill(self) -> None:
        '''
        Load widgets.
        '''
        self._projects_frame = tk.Frame(self, bg=self._colors["bg"])
        self._projects_frame.grid(row=0, column=0, padx=0, pady=0, sticky="NW")

        new_frame = tk.Frame(self._projects_frame, bg=self._colors["bg"])
        new_frame.grid(row=0, column=0, pady=4, sticky="NW")
        self._next_row += 1
        self._new_prj_var = tk.StringVar()
        self._new_prj_entry = ttk.Entry(new_frame, textvariable=self._new_prj_var,
            font=("Sans", 11), width=17, takefocus=0)
        self._new_prj_entry.grid(row=0, column=0, padx=(0, 6))
        self._new_prj_var.set("New Project")
        self._new_prj_btn = ttk.Button(new_frame, text="Add",
            command=self._add_project, width=11, takefocus=0)
        self._new_prj_btn.grid(row=0, column=1)

        self.columnconfigure(1, weight=1)
        btns_frame = tk.Frame(self, bg=self._colors["bg"])
        btns_frame.grid(row=0, column=1, padx=0, pady=0, sticky="NE")
        details_btn = ttk.Button(btns_frame, text="Details",
            command=self.root.main_frame.switch_details_frame, width=8, takefocus=0)
        details_btn.grid(row=0, column=0, pady=(4, 1))
        import_btn = ttk.Button(btns_frame, text="Import",
            command=self._import_projects, width=8, takefocus=0)
        import_btn.grid(row=1, column=0, pady=1)
        export_btn = ttk.Button(btns_frame, text="Export",
            command=self._export_projects, width=8, takefocus=0)
        export_btn.grid(row=2, column=0, pady=1)

        self._fill_projects()

    def _fill_projects(self) -> None:
        '''
        Draw projects fields.
        '''
        projects = self.root.dbase.get_projects()
        if projects:
            for project in projects:
                self._draw_project(project, self._next_row)
                self._next_row += 1

    def _draw_project(self, project, row: int) -> None:
        '''
        Draw roject frame.
        '''
        frame = tk.Frame(self._projects_frame, bg=self._colors["bg"])
        frame.grid(row=row, column=0, pady=1, sticky="NW")
        title = ttk.Label(frame, text=project.title, width=20)
        title.grid(row=0, column=0, pady=0, sticky="NW")
        del_btn = ttk.Button(frame, text="Del", command=self._del_project(
            project, frame), style="Del.TButton", width=4, takefocus=0)
        del_btn.grid(row=0, column=1, padx=2, sticky="NW")
        current = self.root.current_project
        set_btn = ttk.Button(frame, text="Set", width=6)
        set_btn.configure(command=self._set_project(
            project, title, set_btn, del_btn))
        set_btn.grid(row=0, column=2, sticky="NW")
        if project.current:
            self._prev_prj_del_btn = del_btn
            self._prev_prj_set_btn = set_btn
            self._prev_prj_title = title
            del_btn.state(["disabled"])
            set_btn.state(["disabled"])
            title.configure(style="Info.TLabel")

    def _add_project(self) -> None:
        '''
        Add new project.
        '''
        prj_title = self._new_prj_var.get().strip()
        if len(prj_title) < 2:
            tk.messagebox.showerror(title="Error",
                message="Expected at least two characters for a project name.")
            return
        project = self.root.dbase.add_project(prj_title)
        if not self.root.dbase.success:
            return
        self._draw_project(project, self._next_row)
        self._next_row += 1

    def _del_project(self, project, frame) -> callable:
        '''
        Wrapper for button command (deleting particular project).
        '''
        def wrapper() -> None:
            '''
            Delete particular project.
            '''
            if self.root.current_project.dbid == project.dbid:
                return
            confirm = tk.messagebox.askyesno(title="Delete project",
                message=''.join(["Delete '", project.title, "'?"]))
            if not confirm:
                return
            self.root.dbase.del_project(project)
            frame.destroy()
        return wrapper

    def _set_project(self, project, title, set_btn, del_btn) -> None:
        '''
        Wrapper for button command (set project as current).
        '''
        dbase = self.root.dbase
        def wrapper() -> None:
            '''
            Set project as current.
            '''
            if self._prev_prj_del_btn:
                self._prev_prj_del_btn.state(["!disabled"])
            if self._prev_prj_set_btn:
                self._prev_prj_set_btn.state(["!disabled"])
            if self._prev_prj_title:
                self._prev_prj_title.configure(style="TLabel")
            del_btn.state(["disabled"])
            set_btn.state(["disabled"])
            title.configure(style="Info.TLabel")

            self.root.main_frame.stop_project()
            dbase.switch_current_project(self.root.current_project)
            self.root.current_project = project
            dbase.switch_current_project(project)
            self.root.current_time = dbase.load_current_time(project.dbid)
            self._prev_prj_set_btn = set_btn
            self._prev_prj_del_btn = del_btn
            self._prev_prj_title = title
            self._update_frames()
        return wrapper

    def _update_frames(self) -> None:
        '''
        Update data in main and details frames.
        '''
        main_frame = self.root.main_frame
        dframe = self.root.details_frame
        cur_title = self.root.current_project.title
        cur_time = self.root.current_time.seconds
        main_frame.current_time = main_frame.show_time = cur_time
        main_frame.project_name.configure(text=cur_title)
        main_frame.show_count.configure(text=format_seconds(cur_time))
        if dframe.active:
            main_frame.hide_edit_frame()
            dframe.update_view()

    def _import_projects(self) -> None:
        '''
        Import projects from text file with simple format:
        "project name",date,seconds.
        '''
        filename = fd.askopenfilename( title = 'Open a file',
            filetypes = [("Text files", "*.txt")])
        if not filename:
            return
        with open(filename, "r") as f:
            data = f.readlines()
        prj_db_titles = [x.title for x in self.root.dbase.get_projects()]
        try:
            projects_dict = parse_import_file(data, prj_db_titles)
        except (ValueError, TypeError, IndexError) as err:
            tk.messagebox.showerror(title="Error", message=str(err))
            return
        new_projects = self.root.dbase.add_projects_dict(projects_dict)
        for project in new_projects:
            self._draw_project(project, self._next_row)
            self._next_row += 1

    def _export_projects(self) -> None:
        '''
        Export all projects to a text file with simple format:
        "project name",date,seconds.
        '''
        types = [("Text files", "*.txt")]
        file = fd.asksaveasfile(mode='w',filetypes = types,
            initialfile = "export.txt", defaultextension = types)
        if not file:
            return
        projects_titles = {x.dbid: x.title for x in self.root.dbase.get_projects()}
        times = self.root.dbase.get_all_times()
        if not projects_titles or not times:
            tk.messagebox.showerror(title="Export error",
                message="No projects or projects times found!")
            return
        data_list = [','.join([projects_titles[ptime.project_id],
            str(ptime.tdate), str(ptime.seconds)]) for ptime in times]
        data_list.sort()
        data = '\n'.join(data_list)
        try:
            file.write(data)
        except Exception as err:
            tk.messagebox.showerror(title="Export error", message=str(err))
        file.close()

