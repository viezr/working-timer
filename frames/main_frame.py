'''
Main frame.
'''
from time import time
import tkinter as tk
from tkinter import ttk
from threading import Lock

from .utils import format_seconds


class MainFrame(tk.Frame):
    '''
    Main frame class.
    '''
    def __init__(self, root, container):
        self.root, self.container = root, container
        tk.Frame.__init__(self, self.container)
        self._colors = self.root.style.colors
        self.configure(bg=self._colors["bg"])
        self._timer_lock = Lock()
        self._start_time = 0
        self.current_time = 0
        self.show_time = 0
        self._timer_active = False

        self._fill()

    def _fill(self) -> None:
        '''
        Load widgets.
        '''
        cur_title = self.root.current_project.title
        cur_time = self.root.current_time.seconds
        self.current_time = self.show_time = cur_time
        self.project_name = ttk.Label(self, text=cur_title, width=19)
        self.project_name.grid(row=0, column=0, padx=(10, 5), sticky="NS")
        self.show_count = ttk.Label(self, text=format_seconds(cur_time), width=8,
            anchor="center")
        self.show_count.grid(row=0, column=1, padx=5, sticky="NS")
        self._timer_btn = ttk.Button(self, text="Start", takefocus=0,
            command=self.timer_switch, width=6)
        self._timer_btn.grid(row=0, column=2, sticky="NW")
        self._config_btn = ttk.Button(self, text="Config", takefocus=0,
            command=self.switch_config_frame, width=6)
        self._config_btn.grid(row=0, column=3, sticky="NW")
        self.columnconfigure(4, weight=1)
        self._quit_btn = ttk.Button(self, text="X", takefocus=0,
            command=self.quit, width=1)
        self._quit_btn.grid(row=0, column=4, sticky="NE")

    def switch_config_frame(self, *_event) -> None:
        '''
        Open or close config frame.
        '''
        self._config_btn.state(["disabled"])
        config_frame = self.root.config_frame
        if config_frame.active:
            self._hide_frame(config_frame)
            self._hide_frame(self.root.details_frame)
            self._hide_frame(self.root.edit_frame)
        else:
            self._show_frame(config_frame, 1)
        self._config_btn.state(["!disabled"])

    def switch_details_frame(self, *_event) -> None:
        '''
        Open or close config frame.
        '''
        if not self.root.config_frame.active:
            return
        details_frame = self.root.details_frame
        if details_frame.active:
            self._hide_frame(details_frame)
            self._hide_frame(self.root.edit_frame)
        else:
            self._show_frame(details_frame, 2)

    def show_edit_frame(self) -> None:
        '''
        Show time edit frame.
        '''
        if not all((self.root.config_frame, self.root.details_frame)):
            return
        edit_frame = self.root.edit_frame
        if not edit_frame.active:
            self._show_frame(edit_frame, 3)

    def hide_edit_frame(self, *_event) -> None:
        '''
        Hide edit frame.
        '''
        self._hide_frame(self.root.edit_frame)

    def _show_frame(self, frame: tk.Frame, row: int) -> None:
        '''
        Show provided frame.
        '''
        frame.active = True
        frame.grid(row=row, column=0, padx=10, pady=10, sticky="WE")
        frame.update_idletasks()
        if hasattr(frame, "update_view"):
            frame.update_view()

    def _hide_frame(self, frame: tk.Frame) -> None:
        '''
        Hide provided frame.
        '''
        frame.active = False
        frame.grid_remove()

    def quit(self, *_event) -> None:
        '''
        Quit application.
        '''
        with self._timer_lock:
            if self._timer_active:
                self._timer_stop()
        self.root.destroy()

    def stop_project(self) -> None:
        '''
        Public method for stopping running timer.
        '''
        with self._timer_lock:
            if self._timer_active:
                self._timer_stop()

    def timer_switch(self, *_) -> None:
        '''
        Start or stop the timer.
        '''
        with self._timer_lock:
            self._timer_btn.state(["disabled"])
            if self._timer_active:
                self._timer_stop()
            else:
                self._timer_start()
            self._timer_btn.state(["!disabled"])

    def _timer_start(self) -> None:
        '''
        Start timer.
        '''
        self._timer_active = True
        self._start_time = time()
        self._timer_btn.configure(text="Stop", style="Mark.TButton")
        self.after(1000, self._timer_update)

    def _timer_update(self) -> None:
        '''
        Update time label every second.
        '''
        with self._timer_lock:
            if not self._timer_active:
                return
            self.show_time += 1
            htime = format_seconds(self.show_time)
            self.show_count.configure(text=htime)
            self.after(1000, self._timer_update)

    def _timer_stop(self) -> None:
        '''
        Stop timer and save current project time.
        '''
        self._timer_active = False
        self.current_time += int(time() - self._start_time)
        self.show_count.configure(text=format_seconds(self.current_time))
        self._timer_btn.configure(text="Start", style="TButton")
        self._save_current()

    def _save_current(self) -> None:
        '''
        Save current project time.
        '''
        self.root.current_time.seconds = self.current_time
        self.root.dbase.update_item(self.root.current_time,
            {"seconds": self.current_time})
        dframe = self.root.details_frame
        if dframe.active:
            self.root.update_idletasks()
            dframe.update_view()
