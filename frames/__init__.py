'''
Module with frames for graphic interface.
'''
from .main_frame import MainFrame
from .config_frame import ConfigFrame
from .details_frame import DetailsFrame
from .edit_frame import EditFrame
