'''
Edit frame.
'''
from datetime import date
import tkinter as tk
from tkinter import ttk

from .utils import format_seconds, parse_time


class EditFrame(tk.Frame):
    '''
    Edit frame class.
    '''
    def __init__(self, root, container):
        self.root, self.container = root, container
        tk.Frame.__init__(self, self.container)
        self._colors = self.root.style.colors
        self.configure(bg=self._colors["bg"])
        self.active = False
        self.selected_time_dbid = None
        self._project_time = None

        self._fill()

    @property
    def project_time(self):
        return self._project_time

    @project_time.setter
    def project_time(self, project_time) -> None:
        '''
        Set project time for this form and time values for input fileds.
        '''
        self._project_time = project_time
        seconds = format_seconds(project_time.seconds)
        self._htime_var.set(seconds)

    def _fill(self) -> None:
        '''
        Load widgets.
        '''

        self._htime_var = tk.StringVar()
        self._htime_entry = ttk.Entry(self, textvariable=self._htime_var,
            font=("Sans", 11), width=8)
        self._htime_entry.grid(row=0, column=0, padx=(0, 6), sticky="W")

        self.columnconfigure(1, weight=1)
        buttons_frame = tk.Frame(self, bg=self._colors["bg"])
        buttons_frame.grid(row=0, column=1, padx=0, pady=0, sticky="E")

        self._update_btn = ttk.Button(buttons_frame, text="Update",
            command=self._update_time, width=11)
        self._update_btn.grid(row=0, column=0, padx=2, sticky="E")
        self._delete_btn = ttk.Button(buttons_frame, text="Delete",
            command=self._delete_time, width=11)
        self._delete_btn.grid(row=0, column=1, padx=2, sticky="E")

    def _update_time(self) -> None:
        '''
        Update time from input fields.
        '''
        if date.fromisoformat(self._project_time.tdate) == date.today():
            return
        input_htime = self._htime_var.get()
        try:
            seconds = parse_time(input_htime)
        except ValueError:
            return
        if self._project_time.seconds == seconds:
            return
        self._project_time.seconds = seconds
        time_values = self._project_time.get_val_list() + [input_htime,]
        self.root.details_frame.update_selected_item(time_values)
        self.root.dbase.update_item(self._project_time,
            {"seconds": self.project_time.seconds})

    def _delete_time(self) -> None:
        '''
        Delete time from database and frame treeview.
        '''
        if date.fromisoformat(self._project_time.tdate) == date.today():
            return
        mbox = tk.messagebox.askyesno(title="Delete time entry",
            message="Do you really want to delete this entry?")
        if not mbox:
            return
        self.root.details_frame.delete_selected_item()
        self.root.dbase.del_item(self._project_time)
