'''
Details frame.
'''
from datetime import date
import tkinter as tk
from tkinter import ttk

from .utils import format_seconds


class DetailsFrame(tk.Frame):
    '''
    Details frame class.
    '''
    def __init__(self, root, container):
        self.root, self.container = root, container
        tk.Frame.__init__(self, self.container)
        self._colors = self.root.style.colors
        self.configure(bg=self._colors["bg"])
        self.active = False
        self._columns: tuple = (("times_id", "dbid", 50), ("tdate", "Date", 100),
            ("seconds", "Seconds", 80), ("htime", "Time", 80))
        self.project_times = None
        self._selected_item = None

        self._fill()

    def _fill(self) -> None:
        '''
        Load widgets.
        '''
        ytimes_scroll = ttk.Scrollbar(self, orient="vertical")
        ytimes_scroll.grid(row=0, column=1, sticky="NSE")
        self._times_table = ttk.Treeview(self, yscrollcommand=ytimes_scroll.set,
            takefocus=0)
        ytimes_scroll.config(command=self._times_table.yview)
        self._times_table.tag_configure("odd", background=self._colors["bg"])
        self._times_table.tag_configure("even", background=self._colors["bg_light"])
        self._times_table["columns"] = [x[0] for x in self._columns]
        # format columns
        self._times_table.column("#0", width=40, anchor="w", stretch="no")
        for col in self._columns:
            self._times_table.column(col[0], anchor="w", width=col[2], stretch=0)
        # Create Headings
        self._times_table.heading("#0", text="#", anchor="center")
        for head in self._columns:
            self._times_table.heading(head[0], text=head[1], anchor="center")
        self._times_table.grid(row=0, column=0, sticky="NSWE")

        total_frame = tk.Frame(self, bg=self._colors["bg"])
        total_frame.grid(row=1, column=0, columnspan=2, pady=(10, 0), sticky="WE")
        total_frame.columnconfigure(0, weight=1)
        self._show_total = ttk.Label(total_frame, text="", anchor="center")
        self._show_total.grid(row=0, column=0, padx=0, pady=0, sticky="WE")

        copy_btn = ttk.Button(total_frame, text="Copy",
            command=self._copy_to_clipboard, width=5)
        copy_btn.grid(row=0, column=1, padx=0, sticky="E")

        self._times_table.bind("<ButtonRelease-1>", self._time_selected)
        self.update_view()

    def _copy_to_clipboard(self, event: object = None) -> None:
        '''
        Copy selected times data to clipboard.
        '''
        selected_items = self._times_table.selection()
        out_list = []
        title = self.root.current_project.title
        for item in selected_items:
            _dbid, date, time, _htime = self._times_table.item(item)["values"]
            out_list.append(f"{title},{date},{time}")
        out_list.append(self._show_total.cget("text"))
        text = '\n'.join(out_list)
        self.clipboard_clear()
        self.clipboard_append(text)

    def _time_selected(self, _event) -> None:
        '''
        Open frame for updating or deleting time item.
        '''
        self._selected_item = self._times_table.selection()[0]
        cur_item_data = self._times_table.item(self._selected_item)
        item_dbid = cur_item_data["values"][0]
        project_time = [x for x in self.project_times if x.dbid == item_dbid][0]
        if date.fromisoformat(project_time.tdate) == date.today():
            self.root.main_frame.hide_edit_frame()
            return
        self.root.main_frame.show_edit_frame()
        self.root.edit_frame.project_time = project_time

    def update_selected_item(self, time_values: [tuple, list]) -> None:
        '''
        Update selected time item with provided values.
        '''
        self._times_table.item(self._selected_item, values=time_values)

    def delete_selected_item(self) -> None:
        '''
        Remove selected time item from treeview.
        '''
        self._times_table.delete(self._selected_item)

    def update_view(self) -> None:
        '''
        Get times for current project and update times table.
        '''
        self._clear_times_table()
        self._fill_times_table()
        self._set_total_label()

    def _clear_times_table(self) -> None:
        '''
        Clear times table for current project.
        '''
        table_children = self._times_table.get_children()
        if table_children:
            self._times_table.delete(*table_children)

    def _fill_times_table(self) -> None:
        '''
        Fill times table for current project.
        '''
        self.project_times = self.root.dbase.get_project_times(
            self.root.current_project.dbid)
        if not self.project_times:
            return
        for i, time in enumerate(self.project_times):
            row_values = time.get_val_list()
            row_values = row_values + [format_seconds(row_values[2]),]
            self._times_table.insert(parent='', index="end", iid=i, text=i,
                values=row_values, tags=("even" if i % 2 == 0 else "odd",))
        self._times_table.update()

    def _set_total_label(self) -> None:
        '''
        Set total time label for current project.
        '''
        total = self.root.dbase.get_project_total(self.root.current_project.dbid)
        self._show_total.configure(text=f"Total project time: {total} hours, "
            f"{len(self.project_times)} days")
