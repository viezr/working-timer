'''
Database operations interface
'''
import os
import datetime as dt

from models import Project, ProjectTime
from modules.sqlite import Db


class DbaseInterface():
    '''
    Class for database interface
    '''
    def __init__(self):
        self._db = None
        self._db_file = "wtimer.db"
        self._success = False # Public for checking successful transaction

    @property
    def success(self) -> bool:
        '''
        Return current transaction status and reset it.
        '''
        status = self._success
        self._success = False
        return status

    def init_db(self) -> None:
        '''
        Initial database.
        '''
        if os.path.exists(self._db_file):
            self._db = Db(self._db_file)
            self._clean_database()
        else:
            self._db = Db(self._db_file)
            for Model in (Project, ProjectTime):
                self._db.create_table(Model)
            self._db.commit()
        self.add_project("New project", current=True)

    def get_projects(self) -> [list, None]:
        '''
        Main query to get projects.
        '''
        projects = self._db.select_model(Project)
        return projects

    def get_project(self, title: str = None, dbid: int = None) -> [Project, None]:
        '''
        Get project by title or id.
        '''
        if dbid:
            where = ''.join(["project_id = ", str(dbid)])
        elif title:
            where = ''.join(["title = ", "'", str(title), "'"])
        else:
            raise ValueError("No id or title for project provided.")
        project = self._db.select_model(Project, where=where)
        if project:
            return project[0]
        return None

    def add_project(self, title: str, current: bool = False) -> [Project, None]:
        '''
        Add project with provided title.
        '''
        self._success = False
        project = self.get_project(title=title)
        if project:
            return project
        time_created = str(dt.datetime.now(dt.timezone.utc))
        project = Project(title=title, time_created=time_created,
            current=int(current))
        try:
            self._db.insert_model(project)
        except Exception as err:
            self._db.rollback()
            raise err
        self._db.commit()
        self._success = True
        project.project_id = self.get_last_row()
        return project

    def switch_current_project(self, project: Project) -> None:
        '''
        Set project as current.
        '''
        self._check_input_type(project, (Project,))
        self.update_item(project, {"current": int(not bool(project.current))})

    def load_current_project(self) -> [Project, None]:
        '''
        Load project as current.
        '''
        project = self._db.select_model(Project, where="current = 1")
        if project:
            if len(project) > 1:
                for other_prj in project[1:]:
                    self.update_item(other_prj, {"current": 0})
            return project[0]
        project = self._db.select_model(Project, where="project_id = 1")
        if not project:
            raise IndexError("Project id 1 not found for setting as current.")
        project = project[0]
        self.switch_current_project(project)
        project.current = 1
        return project

    def load_current_time(self, project_id: int) -> ProjectTime:
        '''
        Load current project time.
        '''
        self._check_input_type(project_id, (int,))
        cur_date = dt.date.today()
        where = ''.join(["project_id = ", str(project_id),
            " and tdate = '", str(cur_date), "'"])
        result = self._db.select_model(ProjectTime, where=where)
        if not result:
            ptime = self.add_time_item(cur_date, project_id)
            return ptime
        if len(result) > 1:
            err = ''.join(["Found more than 1 current project time for",
                str(cur_date)])
            raise IndexError(err)
        return result[0]

    def get_project_times(self, project_id: int) -> [list, None]:
        '''
        Get project's times.
        '''
        self._check_input_type(project_id, (int,))
        where = ''.join(["project_id = ", str(project_id)])
        result = self._db.select_model(ProjectTime, where=where, order="tdate",
            sort_="DESC")
        return result

    def get_project_total(self, project_id: int) -> int:
        '''
        Get total project hours.
        '''
        self._check_input_type(project_id, (int,))
        query = ''.join(["SELECT SUM(seconds) FROM times WHERE project_id = ",
            str(project_id), ";"])
        total = round(self._db.execute(query)[0]["SUM(seconds)"] / 3600, 2)
        return total

    def get_all_times(self) -> [list, None]:
        '''
        Get all times entires.
        '''
        result = self._db.select_model(ProjectTime)
        return result

    def get_time(self, tdate: dt.datetime) -> [ProjectTime, None]:
        '''
        Get project's times.
        '''
        where = ''.join(["tdate = ", str(tdate)])
        ptime = self._db.select_model(ProjectTime, where=where)
        if ptime:
            return ptime[0]
        return None

    def add_time_item(self, ptime_date: dt.date, project_id: int,
        seconds: int = 0) -> ProjectTime:
        '''
        Add new project's time row.
        '''
        self._check_input_type(project_id, (int,))
        self._check_input_type(seconds, (int,))
        self._success = False
        ptime = self.get_time(ptime_date)
        if ptime:
            return ptime
        ptime = ProjectTime(tdate=ptime_date, seconds=seconds,
            project_id=project_id)
        try:
            self._db.insert_model(ptime)
        except Exception as err:
            self._db.rollback()
            raise err
        self._db.commit()
        ptime.times_id = self.get_last_row()
        self._success = True
        return ptime

    def get_last_row(self) -> [str, int]:
        '''
        Get last row id for multiple transactions
        depended on previous insert
        '''
        return self._db.last_row()

    def update_item(self, item, kwargs) -> None:
        '''
        Update by object
        '''
        self._check_input_type(item, (Project, ProjectTime))
        self._db.update_model(item, **kwargs)
        self._db.commit()

    def del_item(self, item) -> None:
        '''
        Delete by object
        '''
        self._check_input_type(item, (Project, ProjectTime))
        self._db.delete_model(item)
        self._db.commit()

    def del_project(self, project) -> None:
        '''
        Delete project and all it's times.
        '''
        self._check_input_type(project, Project)
        query = ''.join(["DELETE FROM times WHERE project_id = ",
            str(project.project_id), ";"])
        self._db.execute(query)
        self._db.delete_model(project)
        self._db.commit()

    def _clean_database(self) -> None:
        '''
        Clean database from zero times entries.
        '''
        self._db.execute("DELETE FROM times WHERE seconds = 0;")
        self._db.commit()

    def add_projects_dict(self, projects: dict) -> list:
        '''
        Add projects with times from provided dictionary.
        '''
        added_projects = []
        if not projects:
            return added_projects
        for title, times in projects.items():
            project = self.add_project(title)
            if not self._success:
                continue
            for pdate, sec in times.items():
                self.add_time_item(pdate, project.dbid, sec)
                if not self._success:
                    continue
            added_projects.append(project)
        return added_projects

    @staticmethod
    def _check_input_type(item, classes: tuple) -> None:
        if not isinstance(item, classes):
            err = ' '.join(["Expected", str(classes), "type, got",
                str(type(item))])
            raise TypeError(err)
