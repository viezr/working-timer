'''
Project model for SQLite
'''


class Project():
    '''
    Project model for database
    '''
    __tablename__ = "projects"
    __primary_key__ = "project_id"
    __unique_key__ = None # if necessary append "UNIQUE" to key
    project_id = "INTEGER PRIMARY KEY NOT null"
    title = "TEXT NOT null"
    time_created = "TEXT NOT null"
    current = "INTEGER DEFAULT 0 NOT null"

    def __init__(self, **kwargs):
        '''
        Set object attributes as class key = init kwarg.
        '''
        for key, val in kwargs.items():
            if key in __class__.__dict__:
                setattr(self, key, val)

    @property
    def dbid(self):
        '''
        Return database id.
        '''
        return self.project_id

    def __str__(self):
        '''
        Return string with object description.
        '''
        return ''.join(["Project object dbid: ", str(self.dbid),
            " Title '", self.title, "' ",
            "Is" if bool(self.current) else "Not", " current."])
