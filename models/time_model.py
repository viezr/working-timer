'''
Project's time model for SQLite
'''


class ProjectTime():
    '''
    Project's time model for database
    '''
    __tablename__ = "times"
    __primary_key__ = "times_id"
    __unique_key__ = None # if necessary append "UNIQUE" to key
    times_id = "INTEGER PRIMARY KEY NOT null"
    tdate = "TEXT NOT null"
    seconds = "INTEGER DEFAULT 0"
    project_id = " ".join(["INTEGER REFERENCES projects (project_id)",
        "ON DELETE CASCADE ON UPDATE CASCADE"])

    def __init__(self, **kwargs):
        '''
        Set object attributes as class key = init kwarg.
        '''
        for key, val in kwargs.items():
            if key in __class__.__dict__:
                setattr(self, key, val)

    @property
    def dbid(self):
        '''
        Return database id.
        '''
        return self.times_id

    def get_val_list(self) -> list:
        '''
        Return tuple of project time values.
        '''
        return [self.times_id, self.tdate, self.seconds]

    def __str__(self):
        '''
        Return string with object description.
        '''
        return ' '.join(["Time object dbid:", str(self.dbid),
            "Date:", str(self.tdate), "Time:", str(self.seconds),
            "Project:", str(self.project_id)])
