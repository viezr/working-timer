'''
Styles for appllication
'''
import tkinter as tk
from tkinter import ttk
from pathlib import Path


class Style(ttk.Style):
    '''
    Main class for application style.
    '''
    colors = {
        "fg": "#ffffff",
        "bg": "#272c33",
        "bg_mid": "#2c313a",
        "bg_light": "#3f4652",
        "select_dark": "#143b52",
        "select": "#2e8bc0",
        "select_light": "#d4f1f4",
        "select_mid": "#b1d4e0",
        "accent_bg_info": "#2e8bc0",
        "accent_bg_good": "#59981a",
        "accent_bg_bad": "#ffaebc"
    }

    def __init__(self):
        ttk.Style.__init__(self)

        self.colors=__class__.colors
        self.map(".", background=[("disabled", self.colors["bg_mid"])])

        self.configure(".",
            background=self.colors["bg"],
            foreground=self.colors["fg"],
            troughcolor=self.colors["bg"],
            selectbg=self.colors["select"],
            selectfg=self.colors["fg"],
            fieldbg=self.colors["bg"],
            font=("TkDefaultFont",),
            borderwidth=1,
            bordercolor=self.colors["bg"],
            focuscolor=self.colors["select"]
        )

        self._configure_buttons()
        self._configure_labels()
        self._configure_entries()
        self._configure_combobox()
        self._configure_treeview()
        self._configure_scrollbar()
        self._configure_progressbar()

    def _configure_buttons(self) -> None:
        '''
        Configure buttons style.
        '''
        self.configure("TButton", padding=(4, 0, 4, 0), width=12,
            anchor="center", relief="flat", font=("Sans","10"))
        self.configure("Del.TButton", padding=0)
        self.configure("Field.TButton", padding=(2, 0, 2, 0), width=6)
        self.map("TButton",
            background=[
                ("active", self.colors["select"]),
                ("!focus", self.colors["bg_light"]),
                ("focus", self.colors["select"])
            ]
        )
        self.map("Mark.TButton",
            background=[
                ("active", self.colors["accent_bg_good"]),
                ("!focus", self.colors["accent_bg_good"]),
                ("focus", self.colors["accent_bg_good"])
            ]
        )
        self.map("Bad.TButton",
            foreground=[
                ("active", self.colors["bg"])
            ],
            background=[
                ("active", self.colors["accent_bg_bad"]),
                ("!focus", self.colors["bg_light"]),
                ("focus", self.colors["bg_light"])
            ]
        )

        self.configure("TCheckbutton", padding=(10, 4, 10, 4))
        self.configure("Rating.TCheckbutton", padding=(0, 0, 12, 0))

    def _configure_labels(self) -> None:
        '''
        Configure labels style.
        '''
        self.configure("TLabel", background=self.colors["bg"], font=("Sans","10"))
        self.configure("Title.TLabel", font=("Sans","10","bold"))

        self.map("Info.TLabel",
            foreground=[
                ("!focus", self.colors["accent_bg_info"])
            ]
        )
        self.map("Good.TLabel",
            background=[
                ("!focus", self.colors["accent_bg_good"])
            ]
        )
        self.map("Bad.TLabel",
            foreground=[
                ("!focus", self.colors["bg"])
            ],
            background=[
                ("!focus", self.colors["accent_bg_bad"])
            ]
        )

    def _configure_entries(self) -> None:
        '''
        Configure entries style.
        '''
        self.configure("TEntry",
            foreground=self.colors["bg"]
        )
        self.map("TEntry",
            fieldbackground=[
                ("disabled", self.colors["bg_mid"])
            ],
            foreground=[
                ("disabled", self.colors["fg"])
            ]
        )

        self.map("Bad.TEntry",
            fieldbackground=[
                ("!focus", self.colors["accent_bg_bad"])
            ]
        )

    def _configure_combobox(self) -> None:
        '''
        Configure combobox style.
        '''
        self.configure("TCombobox",
            background=self.colors["bg_light"],
            foreground=self.colors["bg"]
        )
        self.map("TCombobox",
            background=[
                ("active", self.colors["select"]),
                ("selected", self.colors["select"])
            ],
            foreground=[
                ("active", self.colors["select"]),
                ("selected", self.colors["select"])
            ]
        )

    def _configure_treeview(self) -> None:
        '''
        Configure treeview style.
        '''
        self.configure("Treeview", relief="flat", borderwidth=0,
            background=self.colors["bg"],
            foreground=self.colors["fg"])

        self.configure("Treeview.Item", padding=(2, 0, 0, 0), relief="flat")
        self.configure("Treeview.Heading", borderwidth=1, relief="groove",
            bordercolor=self.colors["bg"])

        self.map("Treeview",
            background=[
                ("focus", self.colors["select"]),
                ("selected", self.colors["select_dark"])
            ]
        )

    def _configure_scrollbar(self) -> None:
        '''
        Configure scrollbar style.
        '''
        self.configure("Vertical.TScrollbar",
            background=self.colors["bg_light"],
        )

    def _configure_progressbar(self) -> None:
        '''
        Configure progressbar style.
        '''
        self.configure("Horizontal.TProgressbar",
            background=self.colors["select"])
