# Working timer
Simple time-tracker app that shows how much you worked on projects.
Written in Python.  
![image](screenshot.png)  

## Features
- Add/remove/switch projects.
- Show detailed data for a project.
- Import/export projects data.
- Update/delete time items for project.
- Copy selected time items to clipboard.

## Shortcuts:
- Enter - Start/Stop timer.
- C - Open/close config frame.
- I - Open/close details frame.
- Ctrl-Q - quit.
- J, K - Select project in config frame.
- L - Switch to selected project in config frame.

## Notes
For storing data used SQLite database.  
Stop timer, quit app or switch project save current project time.  
By default import/export filetype is text, lines format: "project name,
date iso, seconds".
