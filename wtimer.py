'''
Timer main GUI module.
'''
import tkinter as tk

from style import Style
from dbase_iface import DbaseInterface
from frames import MainFrame, ConfigFrame, DetailsFrame, EditFrame


class Timer(tk.Tk):
    '''
    Timer root GUI class.
    '''
    def __init__(self, dbase: DbaseInterface):
        self._wtitle = "Working timer"
        tk.Tk.__init__(self, className=self._wtitle)
        self.title(self._wtitle)
        self.attributes("-topmost", True)
        self.style = Style()
        self._colors = self.style.colors
        self.configure(background=self._colors["bg"])
        self.dbase = dbase
        self.current_project = dbase.load_current_project()
        self.current_time = dbase.load_current_time(self.current_project.dbid)
        self.columnconfigure(0, weight=1)

        self.main_frame = MainFrame(self, self)
        self.main_frame.grid(row=0, column=0, padx=0, pady=0, sticky="WE")
        self.config_frame = ConfigFrame(self, self)
        self.details_frame = DetailsFrame(self, self)
        self.edit_frame = EditFrame(self, self)

        self.bind("<Control-q>", self.main_frame.quit)
        self.bind("<Key-c>", self.main_frame.switch_config_frame)
        self.bind("<Key-i>", self.main_frame.switch_details_frame)
        self.bind("<Return>", self.main_frame.timer_switch)
        self.bind("<Key-k>", lambda x: self.event_generate("<Shift-Tab>"))
        self.bind("<Key-j>", lambda x: self.event_generate("<Tab>"))
        self.bind("<Key-l>", lambda x: self.event_generate("<space>"))


if __name__ == "__main__":
    dbase = DbaseInterface()
    dbase.init_db()
    timer = Timer(dbase)
    timer.mainloop()
